package demo2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.FilterType

@SpringBootApplication(exclude = [
    WebMvcAutoConfiguration::class
])
@ComponentScan(value = ["demo2.repo", "demo2.service"],
        excludeFilters = [
            ComponentScan.Filter(type = FilterType.REGEX, pattern = ["demo2\\.web"])
        ])
class TestsRunConfig
