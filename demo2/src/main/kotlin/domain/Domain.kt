package demo2.domain

import java.time.LocalDateTime

typealias ID = Int

data class Project(
        val id: ID,
        val name: String,
        val isActive: Boolean = true,
        var description: String? = null,
        val identifier: String = "",
        var lastModified: LocalDateTime? = null,
        var startDate: String = "",
        var endDate: String = "",
        var ownerId: ID? = null,
        var email: String? = null
)

data class Task(
        val id: ID,
        val title: String,
        var description: String? = null,
        val colorId: String,
        val position: Int,
        val isActive: Boolean = true,
        val dateCreation: LocalDateTime = LocalDateTime.now(),
        var dateStarted: LocalDateTime? = null,
        var dateCompleted: LocalDateTime? = null,
        var dateDue: LocalDateTime? = null,
        var dateModification: LocalDateTime? = null,
        var dateMoved: LocalDateTime? = null,
        val priority: Int = 0,
        val projectId: ID = 0,
        val columnId: ID = 0,
        val swimlaneId: ID = 0,
        val creatorId: ID = 0,
        var categoryId: ID? = null,
        var ownerId: ID? = null)

data class TaskExternalLink(
        val id: ID,
        val linkType: String,
        val title: String,
        val url: String,
        val dateCreation: LocalDateTime,
        val dateModification: LocalDateTime,
        val creatorId: ID)

data class Column(
        val id: ID,
        val title: String,
        val position: Int,
        val projectId: ID,
        val taskLimit: Int? = null,
        val hideInDashboard: Boolean = false)

data class Swimlane(
        val id: ID,
        val name: String,
        val position: Int,
        val projectId: ID)

data class Comment(
        val id: ID,
        val taskId: ID,
        val userId: ID,
        val comment: String,
        val reference: String? = null,
        val dateCreation: LocalDateTime,
        val dateModification: LocalDateTime)

data class User(
        val id: ID,
        val username: String,
        var password: String?,
        val isAdmin: Boolean,
        val isLdapUser: Boolean,
        val name: String? = null,
        val email: String? = null,
        val googleId: String? = null,
        val githubId: String? = null,
        val notificationsEnabled: Boolean = false,
        val timezone: String? = null,
        val language: String? = null,
        val disableLoginForm: Boolean = false,
        val twofactorActivated: Boolean = false,
        val twofactorSecret: String? = null,
        val token: String? = null,
        val notificationsFilter: Int? = null,
        val isProjectAdmin: Boolean = false,
        val gitlabId: ID? = null,
        val role: String? = "app-user",
        val active: Boolean = true,
        val avatarPath: String? = null,
        val apiAccessToken: String? = null,
        val filter: String? = null)

data class TaskUser(
        val id: ID,
        val username: String,
        val name: String,
        val email: String,
        var avatarPath: String? = null)

data class ProjectBag(
        val project: Project = Project(0, ""),
        val tasks: Collection<Task> = listOf(),
        val columns: Collection<Column> = listOf(),
        val swimlanes: Collection<Swimlane> = listOf())

data class TaskBag(
        val project: Project,
        val task: Task,
        val comments: Collection<Comment> = listOf(),
        val users: Collection<TaskUser> = listOf(),
        val links: Collection<TaskExternalLink> = listOf())

data class FilteringRequest(
        val projectId: ID? = null,
        val taskId: ID? = null,
        val userId: ID? = null,
        val isActive: Boolean? = null,
        val keyword: String? = null,
        val lastUpdated: LocalDateTime? = null,
        val searchInDescrition: Boolean = false,
        val withDescriptions: Boolean = false)
