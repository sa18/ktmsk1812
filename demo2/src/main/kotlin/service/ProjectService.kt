package demo2.service

import demo2.repo.ProjectRepo
import org.springframework.stereotype.Service

@Service
class ProjectService(private val projRepo: ProjectRepo) {

    fun listProjects() = projRepo.listProjectsAsMap()

}