package demo3.web

import demo3.service.ProjectService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/api/v1")
class DataController(private var projSrv: ProjectService) {

    @Autowired(required = false)
    private lateinit var request: HttpServletRequest

    @GetMapping("/projects")
    fun getProjects() = mapOf("projects" to projSrv.listProjects())

}