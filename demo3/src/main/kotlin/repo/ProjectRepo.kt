package demo3.repo

import demo3.domain.ID
import demo3.domain.Project
import demo3.util.logger
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository

@Repository
class ProjectRepo(private val db: JdbcTemplate)  {

    fun findById(id: ID): Project {
        return findByIdAny(id) ?: throw NoSuchElementException("Project $id not found")
    }

    fun findByIdAny(id: ID): Project? {
        return db.queryForList("select * from projects where id = ?", Project::class.java, id)
                .firstOrNull()
    }

    fun listProjectsAsMap(): List<Map<String, Any>> {
        return db.queryForList("select id, name, is_active from projects order by id")
    }

    companion object {
        private val log by logger()
    }
}
