package demo3.service

import demo3.domain.ID
import demo3.domain.ProjectBag
import demo3.repo.ColumnRepo
import demo3.repo.ProjectRepo
import demo3.repo.SwimlaneRepo
import demo3.repo.TaskRepo
import org.springframework.stereotype.Service

@Service
class ProjectService {

    private lateinit var projRepo: ProjectRepo
    private lateinit var taskRepo: TaskRepo
    private lateinit var columnRepo: ColumnRepo
    private lateinit var swimlaneRepo: SwimlaneRepo

    fun listProjects() = projRepo.listProjectsAsMap()

    fun getProjectBag(projectId: ID) : ProjectBag {
        val prj = projRepo.findById(projectId)
        val tasks = taskRepo.listByProject(projectId)
        val columns = columnRepo.listByProject(projectId)
        val swimlanes = swimlaneRepo.listByProject(projectId)
        return ProjectBag(prj, tasks, columns, swimlanes)
    }
}
