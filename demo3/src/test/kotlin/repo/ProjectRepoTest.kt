package demo3.repo

import demo3.TestsRunConfig
import demo3.util.logger
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
@ContextConfiguration(classes = [TestsRunConfig::class])
class ProjectRepoTest {

    private val log by logger()

    @Autowired
    private lateinit var projectRepo: ProjectRepo

    @Test
    fun test() {
        log.info(projectRepo.listProjectsAsMap().toString())
    }

}