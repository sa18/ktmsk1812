package demo3

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.FilterType

@SpringBootApplication(exclude = [
    WebMvcAutoConfiguration::class
])
@ComponentScan(value = ["demo3.repo", "demo3.service"],
        excludeFilters = [
            ComponentScan.Filter(type = FilterType.REGEX, pattern = ["demo3\\.web"])
        ])
class TestsRunConfig
